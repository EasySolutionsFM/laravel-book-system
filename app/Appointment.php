<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Appointment extends Pivot
{
    protected $table= "appointments";
    protected $fillable = ['duration','booking_date','from','to'];
    
}
