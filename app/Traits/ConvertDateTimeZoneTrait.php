<?php
namespace App\Traits;

use Carbon\Carbon;
use DateTime;

trait ConvertDateTimeZoneTrait
{
    protected function convertTimeZone($date, $from_tz, $to_tz)
    {
        $datetime =  new DateTime($date);
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $datetime->format('Y-m-d H:i:s'), $from_tz);
        return $date->setTimezone($to_tz);
    }
}