<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = ['name','timezone'];
    public function appointments()
    {
        return $this->belongsToMany('App\Expert','appointments','user_id','expert_id')->using('App\Appointment');
    }
}
