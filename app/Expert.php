<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expert extends Model
{
    public function clients()
    {
        return $this->belongsToMany('App\User')->using('App\Appointment');
    }

}
