<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Expert;
use App\Traits\ConvertDateTimeZoneTrait;
use App\User;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;

class AppointmentsController extends Controller
{
    use ConvertDateTimeZoneTrait;
    protected $timezone;
    function __construct() {
        $this->timezone = $_COOKIE['timezone'] ;
    }

    public function display($expert_id){

        $expert = Expert::find($expert_id);
        // Code Review: it's better to use the trait on model level or to use Laravel mutator instead to avoid repeating this convert process eveywhere
        $expert->wrk_hrs_from = $this->convertTimeZone($expert->wrk_hrs_from,'UTC',$this->timezone)->toTimeString();
        $expert->wrk_hrs_to = $this->convertTimeZone($expert->wrk_hrs_to,'UTC',$this->timezone)->toTimeString();

        // get expert reserved appointments
        $appointments = Appointment::where([
            ['booking_date','>=',gmdate("Y-m-d")], 
            ['expert_id' , '=', $expert_id]
        ])->get();
        
        // Code Review: Convert Trait on model level or laravel mutator is a goof replacement for this loop
        for ($i = 0 ; $i< sizeof($appointments);$i++) {
            $appointments[$i]->booking_date= $this->convertTimeZone($appointments[$i]->booking_date,'UTC',$this->timezone)->toDateString();
            $appointments[$i]->from= $this->convertTimeZone($appointments[$i]->from,'UTC',$this->timezone);
            $appointments[$i]->to= $this->convertTimeZone($appointments[$i]->to,'UTC',$this->timezone);
        }
        $data = [
            'expert' => $expert, 
            'timezone' => $this->timezone,
            'appointments' => $appointments
        ];
        return view('book')->with('data',$data);
    }
    public function store(Request $request , $expert_id){
        $this->validate($request, [
            'date' => 'required|date_format:Y-m-d',
            'name' => 'required|string',
            'duration' => 'required|string',
            'timezone' => 'required|string',
            'from' => 'required',
            'to' => 'required',
        ]);

        $user = new User([
            'name' => $request->name,
            'timezone' => $request->timezone
        ]);
        $user->save();
        
        // Code Review: it's better to use the trait on model level or to use Laravel mutator instead to avoid repeating this convert process eveywhere
        $from = $this->convertTimeZone($request->date.' '.$request->from,$this->timezone,'UTC');
        $to = $this->convertTimeZone($request->date.' '.$request->to,$this->timezone,'UTC');
        $booking_date = $this->convertTimeZone($request->date.' '.$request->from,$this->timezone,'UTC');

        $user->appointments()->attach($expert_id,[
            'booking_date' => $booking_date,
            'from' => $from,
            'to' => $to,
            'duration' => $request->duration
        ]);
        $data = [
            'date' => $request->date,
            'from' => $request->from,
            'to' => $request->to
        ];
        return view('finished')->with('data',$data);

    }
}
