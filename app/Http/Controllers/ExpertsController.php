<?php

namespace App\Http\Controllers;

use App\Expert;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Traits\ConvertDateTimeZoneTrait;

class ExpertsController extends Controller
{
    use ConvertDateTimeZoneTrait;
    protected $timezone;
    function __construct() {
         $this->timezone =  $_COOKIE['timezone'];
    }
    
    public function index(Request $request)
    {
          $experts = Expert::all();
          return view('experts')->with('experts',$experts);
    }

      /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function show($id)
    {
        $expert = Expert::find($id);
        // Code Review: it's better to use the trait on model level or to use Laravel mutator instead to avoid repeating this convert process eveywhere
        $expert->wrk_hrs_from = $this->convertTimeZone($expert->wrk_hrs_from,'UTC',$this->timezone)->toTimeString();
        $expert->wrk_hrs_to = $this->convertTimeZone($expert->wrk_hrs_to,'UTC',$this->timezone)->toTimeString();

        return view('show')->with('expert',$expert);
    }

}
