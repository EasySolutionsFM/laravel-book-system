<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExpertSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('experts')->insert([[
            'name' => 'Li Wei',
            'expert' => 'Chinese teacher',
            'country' => 'China',
            'wrk_hrs_from' => '2020-05-10 1:00:00', // time stored into UTC, in china it's from 9:00AM - 5:00 PM
            'wrk_hrs_to' => '2020-05-10 9:00:00',
            'gender' => 'male',
            'timezone' => 'Asia/Hong_Kong',
        ], 
        [
            'name' => 'Quasi Shawa',
            'expert' => 'Civil Engineer',
            'country' => 'Syria',
            'wrk_hrs_from' => '2020-05-10 3:00:00',// time stored into UTC, in syria it's from 6:00AM - 12:00 PM
            'wrk_hrs_to' => '2020-05-10 9:00:00',
            'gender' => 'male',
            'timezone' => 'Asia/Damascus',

        ],
        [
            'name' => 'Shimaa Badawy',
            'expert' => 'Computer Engineer',
            'country' => 'Egypt',
            'wrk_hrs_from' => '2020-05-10 11:00:00',// time stored into UTC, in egypt it's from 1:00PM - 2:00 PM
            'wrk_hrs_to' => '2020-05-10 12:00:00',
            'gender' => 'female',
            'timezone' => 'Africa/Cairo',
            
        ]
        
        ]);

    }
}
