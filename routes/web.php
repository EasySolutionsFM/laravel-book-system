<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home page
Route::get('/', function () {
    return view('welcome');
});


Route::get('/experts', 'ExpertsController@index');

Route::get('/experts/{id}/show', 'ExpertsController@show');

Route::get('/experts/{id}/book', 'AppointmentsController@display');


Route::post('/appointment/{id}', 'AppointmentsController@store');
