@extends('layouts.app')

@section('title', 'Expert Details')


@section('content')

<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="row">
            <div class="card text-center border-dark" style="width: 33rem; border: 11px solid rgba(0, 0, 0, 0.125)">
                <div class="card-body">
                    <div class="col-lg-12">
                        @if ($expert->gender == 'female')
                        <img class="rounded-circle" src="https://img.icons8.com/ios-filled/50/000000/user-female-circle.png" alt="Generic placeholder image" width="140" height="140">
                        @else
                        <img class="rounded-circle" src="https://img.icons8.com/ios-filled/50/000000/user-male-circle.png" alt="Generic placeholder image" width="140" height="140">
                        @endif
                        <h2 class="text-primary">{{$expert->name}}</h2>
                        <p class="text-dark">{{$expert->expert}}</p>
                        <p class="text-dark">Country: {{$expert->country}}</p>
                        <p class="text-dark">Working Hours: {{date("h:i:s a", strtotime($expert->wrk_hrs_from))}} -> {{date("h:i:s a", strtotime($expert->wrk_hrs_to))}}</p>
                    </div>
                    <a href="{{ url('experts/'.$expert->id.'/book') }}" class="btn btn-primary">Book Now</a>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection