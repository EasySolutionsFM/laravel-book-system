@extends('layouts.app')

@section('title', 'Experts')


@section('content')

<div class="flex-center position-ref full-height">
    <div class="content">
    <h1>Your Appointment will be at {{$data['date']}} from {{$data['from']}} to {{$data['to']}}
                        </h1>
    <a role="button" class="btn btn-lg  btn-primary" href="{{ url('/') }}">Back to home</a>
    </div>
</div>

@endsection
