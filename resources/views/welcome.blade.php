@extends('layouts.app')

@section('title', 'Welcome')


@section('content')
    <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Welcome to BookExperts System
                </div>
                <a role="button" class="btn btn-lg  btn-primary" href="{{ url('experts/') }}">Get Started</a>
            </div>
        </div>
@endsection

