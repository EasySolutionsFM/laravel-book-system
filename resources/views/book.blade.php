@extends('layouts.app')

@section('title', 'Experts')


@section('content')
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="row">
            <div class="card text-center border-dark" style="width: 34rem;">
                <div class="card-body">
                    <div class="col-lg-12">
                        <form method="post" action="{{url('/appointment/' . $data['expert']->id)}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="timezone" id="timezone">
                            {{-- Date Picker --}}
                            <div class="form-group">
                                <label for="datepicker" class="pull-left">Date</label>
                                <input type="text" class="date form-control" id="datepicker" name="date" placeholder="Enter a date"> </div>
                    </div>
                    <h6 id="time_zone_text">Timezone: {{$data['timezone']}}</h6>

                    {{-- Name --}}
                    <div class="form-group col-md-12 mt-5">
                        <label for="name" class="pull-left">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name">
                    </div>
                    {{-- Duration --}}
                    <div class="form-group input-group-lg col-md-12">
                        <label for="duration" class="pull-left">Duration</label>
                        <select class="custom-select" id="duration" name="duration">
                            <option selected>Choose...</option>
                            <option value="15">15 min</option>
                            <option value="30">30 min</option>
                            <option value="45">45 min</option>
                            <option value="1">1 hour</option>
                        </select>
                    </div>
                    {{-- From --}}
                    <div class="form-group input-group-lg col-md-6">
                        <label for="from" class="pull-left">From</label>
                        <select class="custom-select" id="from" name="from">
                        </select>
                    </div>
                    {{-- To --}}
                    <div class="form-group input-group-lg col-md-6 mb-5">
                        <label for="to" class="pull-left">To</label>
                        <select class="custom-select" id="to" name="to">
                        </select>
                    </div>

                    {{-- <h6>Your Appointment will be at 2020-05-01 from 11:00 am to 12:00 pm
                        </h6> --}}
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4 mt-5">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
</div>

<script type="text/javascript">
    $('#datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
    $('#datepicker').on('changeDate', function(event) {

        //   alert($('#datepicker').val());
        generateTimeSlot('#from', $('#datepicker').val());
        generateTimeSlot('#to', $('#datepicker').val());

    });
    $(document).ready(function() {
        $('#timezone').val(moment.tz.guess());
        var time_zone = $('#timezone').val();
        // $('#time_zone_text').append(time_zone);
        // document.cookie = "timezone=".time_zone;
        generateTimeSlot('#from', 0);
        generateTimeSlot('#to', 0);
    });

    function generateTimeSlot(select_id, selectedDate = 0) {
        // Get red of the current time slots
        $(select_id).empty();

        // See if there is any reserved appointments in the current selected date
        selectedDate = selectedDate || 0;
        var reserved_found = false;
        var reserved_times = [];
        if (selectedDate != 0) {
            var appointments = '{{$data['appointments']}}';
            @foreach($data['appointments'] as $appointment)
            if (selectedDate == '{{$appointment->booking_date}}') {
                reserved_times.push(['{{$appointment->from}}', '{{$appointment->to}}']);
                reserved_found = true; // it should be javascript param here
            }
            @endforeach
        }
        // append the default option
        $(select_id).append(new Option("Choose...", "", true, true));

        // start bulding other time slots by looping over the day hours (24) and the minuts (60)
        let start = selectedDate + ' ' + '{{$data['expert']->wrk_hrs_from}}';
        let end = selectedDate + ' ' + '{{$data['expert']->wrk_hrs_to}}';
        for (let hours = 0; hours < 24; hours++) {
            for (let mins = 0; mins < 60; mins += 15) {
                let time_string = hours.toString().padStart(2, "0") + ':' + mins.toString().padStart(2, "0") + ':00';
                let date_time = selectedDate + ' ' + time_string;
                let found = false;
                // check for pervious appointments existance
                if (selectedDate != 0 && reserved_times.length > 0) {
                    // if pervious appointments found at the selected date
                    for (let index = 0; index < reserved_times.length; index++) {
                        if (date_time >= reserved_times[index][0] && date_time < reserved_times[index][1]) {
                            found = true;
                            break;
                        }   
                    }
                    if (date_time >= start && date_time <= end && !found) {
                        $(select_id).append(new Option(time_string, time_string, false, false));
                    }
                } else { // No date is determined yet or no appointments at all
                    if (date_time >= start && date_time <= end) {
                        $(select_id).append(new Option(time_string, time_string, false, false));
                    }
                }
            }

        }

    }
</script>
@endsection