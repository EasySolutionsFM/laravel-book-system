@extends('layouts.app')

@section('title', 'Experts')


@section('content')

<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            Choose the expert you want!
        </div>
        <div class="row">
            @foreach ($experts as $expert)
            <div class="col-lg-4">
                @if ($expert->gender == 'female')
                <img class="rounded-circle" src="https://img.icons8.com/ios-filled/50/000000/user-female-circle.png" alt="Generic placeholder image" width="140" height="140">
                @else
                <img class="rounded-circle" src="https://img.icons8.com/ios-filled/50/000000/user-male-circle.png" alt="Generic placeholder image" width="140" height="140">
                @endif
                <h2 class="text-primary">{{$expert->name}}</h2>
                <p class="text-dark">{{$expert->expert}}</p>
                <p><a class="btn btn-secondary" href="{{ url('experts/'.$expert->id . '/show') }}" role="button">More info »</a></p>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection